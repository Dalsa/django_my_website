from django.urls import path, include
from . import views

urlpatterns = [
    path('', views.Post_List.as_view(), name="blog"), #as_view 메소드를 사용해야 에러가 안 나고 잘나옴
    path('<uuid:pk>/', views.PostDetail.as_view() ), #inteager 타입으로 숫자를 pk로 의미한다.
    path('category/<str:slug>/', views.PostListByCategory.as_view()),
    path('tag/<str:slug>/', views.PostListByTag.as_view()),
    path('<uuid:pk>/update/', views.PostUpdate.as_view()),
    path('create/', views.PostCreate.as_view()),
    path('<uuid:pk>/new_comment/', views.new_comment),
    path('delete_comment/<int:pk>/', views.delete_comment),
    path('edit_comment/<int:pk>/', views.CommentUpdate.as_view()),
    path('search/<str:q>/', views.PostSearh.as_view()),
    # path('delete_comment/<int:pk>/', views.CommentDelete.as_view()),
]